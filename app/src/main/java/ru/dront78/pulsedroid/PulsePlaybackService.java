package ru.dront78.pulsedroid;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.RemoteViews;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.ServiceCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import static ru.dront78.pulsedroid.PlayState.BUFFERING;
import static ru.dront78.pulsedroid.PlayState.STARTING;

public class PulsePlaybackService extends Service implements PulsePlaybackWorker.Listener {
    /**
     * Unique ID for the Notification.
     */
    private static final int NOTIFICATION = R.string.playback_service_status;

    private static final String ACTION_TOGGLE = PulsePlaybackService.class.getName() + ".TOGGLE";

    private final IBinder binder = new LocalBinder();

    private Handler handler = new Handler();
    private NotificationManager notifManager;
    private PowerManager.WakeLock wakeLock;
    private PendingIntent togglePendingIntent;

    @Nullable
    private PulsePlaybackWorker playWorker = null;
    @Nullable
    private Thread playWorkerThread;
    private volatile int bufferMillisAhead = 125;
    private volatile int bufferMillisBehind = 1000;

    private final MutableLiveData<PlayState> playState = new MutableLiveData<>();
    private SharedPreferences sharedPrefs;

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        handler = new Handler();
        notifManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        sharedPrefs = getSharedPreferences(PulsePlaybackService.class.getName(), Context.MODE_PRIVATE);

        bufferMillisAhead = sharedPrefs.getInt("buffer_ms_ahead", 125);
        bufferMillisBehind = sharedPrefs.getInt("buffer_ms_behind", 500);

        playState.setValue(PlayState.STOPPED);

        Intent intent = new Intent(this, PulsePlaybackService.class)
                .setAction(ACTION_TOGGLE);
        togglePendingIntent = PendingIntent.getService(
                this, R.id.intent_toggle_service, intent, 0);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "pulsedroid:wakelock");

        if (Build.VERSION.SDK_INT >= 26) {
            notifManager.createNotificationChannel(new NotificationChannel(
                    getString(R.string.service_notification_channel),
                    getString(R.string.playback_service_label),
                    NotificationManager.IMPORTANCE_LOW));
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (ACTION_TOGGLE.equals(intent.getAction())) {
                switch (getPlayState()) {
                    case STOPPED:
                        play(getServerPref(), getPortPref());
                        break;
                    default:
                        stop();
                        break;
                }
            }
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        stop();
    }

    @SuppressLint("InlinedApi")
    @Override
    public void onPlaybackError(@NonNull PulsePlaybackWorker worker, @NonNull Throwable t) {
        if (worker == playWorker) {
            notifyState(PlayState.STOPPED);
            ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_DETACH);
            stopSelf();
        }
    }

    @Override
    public void onPlaybackBuffering(@NonNull PulsePlaybackWorker worker) {
        if (worker == playWorker) {
            notifyState(BUFFERING);
        }
    }

    @Override
    public void onPlaybackStarted(@NonNull PulsePlaybackWorker worker) {
        if (worker == playWorker) {
            notifyState(PlayState.STARTED);
        }
    }

    @Override
    public void onPlaybackStopped(@NonNull PulsePlaybackWorker worker) {
        if (worker == playWorker) {
            notifyState(PlayState.STOPPED);
            ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_DETACH);
            stopSelf();
        }
    }

    @MainThread
    public void play(@NonNull String server, int port) {
        if (!isStartable()) {
            throw new IllegalStateException("Cannot start with playState == " + getPlayState());
        }
        if (playWorker != null) {
            stopWorker();
        }
        playWorker = new PulsePlaybackWorker(server, port, wakeLock, handler, this);
        playWorker.setMaxBufferMillis(bufferMillisAhead, bufferMillisBehind);
        playWorkerThread = new Thread(playWorker);

        startForeground(NOTIFICATION, createNotification(STARTING));

        notifyState(STARTING);

        playWorkerThread.start();

        // allow running in the background when service gets unbound
        startService(new Intent(this, PulsePlaybackService.class));
    }

    @MainThread
    public void stop() {
        if (getPlayState().isActive()) {
            notifyState(PlayState.STOPPING);
        }
        stopWorker();
    }

    @MainThread
    private void stopWorker() {
        if (playWorker != null) {
            playWorker.stop();
        }
        if (playWorkerThread != null) {
            playWorkerThread.interrupt();
        }
    }

    public String getServerPref() {
        return sharedPrefs.getString("server", "");
    }

    public int getPortPref() {
        return sharedPrefs.getInt("port", -1);
    }

    public boolean getAutostartPref() {
        return sharedPrefs.getBoolean("auto_start", false);
    }

    public void setPrefs(String server, int port, boolean checked) {
        sharedPrefs.edit()
                .putString("server", server)
                .putInt("port", port)
                .putBoolean("auto_start", checked)
                .apply();
    }

    public int getBufferAhead() {
        return bufferMillisAhead;
    }

    public int getBufferBehind() {
        return bufferMillisBehind;
    }

    public void setBufferMillis(int bufferSizeAhead, int bufferSizeBehind) {
        this.bufferMillisAhead = bufferSizeAhead;
        this.bufferMillisBehind = bufferSizeBehind;
        if (playWorker != null) {
            playWorker.setMaxBufferMillis(bufferSizeAhead, bufferSizeBehind);
        }
        sharedPrefs.edit()
                .putInt("buffer_ms_ahead", bufferMillisAhead)
                .putInt("buffer_ms_behind", bufferMillisBehind)
                .apply();
    }

    private void notifyState(@NonNull PlayState state) {
        playState.setValue(state);
        notifManager.notify(NOTIFICATION, createNotification(state));
    }

    public boolean isStartable() {
        return getPlayState() == PlayState.STOPPED;
    }

    public PlayState getPlayState() {
        return playState.getValue();
    }

    @NonNull
    public LiveData<PlayState> playState() {
        return playState;
    }

    public Throwable getError() {
        return playWorker == null ? null : playWorker.getError();
    }

    public void showNotification() {
        notifManager.notify(NOTIFICATION, createNotification(getPlayState()));
    }

    private Notification createNotification(@NonNull PlayState state) {
        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, PulseDroidActivity.class), 0);

        int statusResId;
        int buttonResId = R.string.btn_stop;
        switch (state) {
            case STOPPED:
                statusResId = R.string.playback_status_stopped;
                buttonResId = R.string.btn_play;
                break;
            case STARTING:
                statusResId = R.string.playback_status_starting;
                break;
            case BUFFERING:
                statusResId = R.string.playback_status_buffering;
                break;
            case STARTED:
                statusResId = R.string.playback_status_playing;
                break;
            case STOPPING:
                statusResId = R.string.playback_status_stopping;
                break;
            default:
                throw new IllegalArgumentException();
        }

        String contentText = getString(R.string.playback_service_status, getString(statusResId));

        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notif_service);
        contentView.setTextViewText(R.id.toggleButton, getText(buttonResId));
        contentView.setOnClickPendingIntent(R.id.toggleButton, togglePendingIntent);
        contentView.setTextViewText(R.id.contentText, contentText);
        return new NotificationCompat.Builder(this, getString(R.string.service_notification_channel))
                .setCustomContentView(contentView)
                .setContentTitle(getText(R.string.playback_service_label))
                .setContentText(contentText)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_pulse)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .build();
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        PulsePlaybackService getService() {
            return PulsePlaybackService.this;
        }
    }

}
