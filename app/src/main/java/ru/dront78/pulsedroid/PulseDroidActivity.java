package ru.dront78.pulsedroid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;


public class PulseDroidActivity extends AppCompatActivity {

    public static final int DEFAULT_INDEX_AHEAD = 2;
    public static final int DEFAULT_INDEX_BEHIND = 3;
    private static final List<Integer> BUFFER_SIZES_AHEAD = Arrays.asList(0, 64, 125, 250, 500, 1000, 2000);
    private static final List<Integer> BUFFER_SIZES_BEHIND = Arrays.asList(0, 125, 250, 500, 1000, 2000, 5000, 10000, -1);

    private Button playButton = null;
    private Spinner bufferSizeSpinnerAhead;
    private Spinner bufferSizeSpinnerBehind;
    private BufferSizeAdapter bufferSizeAdapterAhead;
    private BufferSizeAdapter bufferSizeAdapterBehind;
    private EditText serverInput;
    private EditText portInput;
    private CheckBox autoStartCheckBox = null;
    private TextView errorText;

    private PulsePlaybackService boundService;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            boundService = ((PulsePlaybackService.LocalBinder) service).getService();

            boundService.playState().observe(PulseDroidActivity.this,
                    playState -> updatePlayState(playState));

            boundService.showNotification();

            updatePrefs(boundService);

            if (boundService.getAutostartPref() && boundService.isStartable()) {
                play();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            boundService.playState().removeObservers(PulseDroidActivity.this);

            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            boundService = null;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        serverInput = findViewById(R.id.EditTextServer);
        portInput = findViewById(R.id.EditTextPort);
        autoStartCheckBox = findViewById(R.id.auto_start);
        playButton = findViewById(R.id.ButtonPlay);
        errorText = findViewById(R.id.errorText);
        bufferSizeSpinnerAhead = findViewById(R.id.bufferSizeSpinnerAhead);
        bufferSizeSpinnerBehind = findViewById(R.id.bufferSizeSpinnerBehind);

        bufferSizeAdapterAhead = new BufferSizeAdapter(this, BUFFER_SIZES_AHEAD);
        bufferSizeAdapterBehind = new BufferSizeAdapter(this, BUFFER_SIZES_BEHIND);

        playButton.setOnClickListener(v -> {
            if (boundService.getPlayState().isActive()) {
                stop();
            } else {
                play();
            }
        });

        bindService(new Intent(this, PulsePlaybackService.class),
                mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        unbindService(mConnection);
        super.onDestroy();
    }

    private void updatePrefs(PulsePlaybackService service) {
        serverInput.setText(service.getServerPref());
        int port = service.getPortPref();
        portInput.setText(port > 0 ? Integer.toString(port) : "");
        autoStartCheckBox.setChecked(service.getAutostartPref());
        setUpSpinner(bufferSizeSpinnerAhead, bufferSizeAdapterAhead, service.getBufferAhead(), DEFAULT_INDEX_AHEAD);
        setUpSpinner(bufferSizeSpinnerBehind, bufferSizeAdapterBehind, service.getBufferBehind(), DEFAULT_INDEX_BEHIND);
    }

    private void setUpSpinner(Spinner spinner, BufferSizeAdapter adapter, int value, int defaultIndex) {
        spinner.setOnItemSelectedListener(null);

        spinner.setAdapter(adapter);

        int pos = adapter.getItemPosition(value);
        spinner.setSelection(pos >= 0 ? pos : defaultIndex);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (boundService != null) {
                    int aheadMillis = bufferSizeAdapterAhead.getItem(bufferSizeSpinnerAhead.getSelectedItemPosition());
                    int behindMillis = bufferSizeAdapterBehind.getItem(bufferSizeSpinnerBehind.getSelectedItemPosition());
                    boundService.setBufferMillis(aheadMillis, behindMillis);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void updatePlayState(@Nullable PlayState playState) {
        if (playState == null) {
            playButton.setText(R.string.btn_waiting);
            playButton.setEnabled(false);
            return;
        }
        switch (playState) {
            case STOPPED:
                playButton.setText(R.string.btn_play);
                playButton.setEnabled(true);
                break;
            case STARTING:
                playButton.setText(R.string.btn_starting);
                playButton.setEnabled(true);
                break;
            case BUFFERING:
                playButton.setText(R.string.btn_buffering);
                playButton.setEnabled(true);
                break;
            case STARTED:
                playButton.setText(R.string.btn_stop);
                playButton.setEnabled(true);
                break;
            case STOPPING:
                playButton.setText(R.string.btn_stopping);
                playButton.setEnabled(false);
                break;
        }
        Throwable error = boundService == null ? null : boundService.getError();
        if (error != null) {
            String text = formatMessage(error);
            errorText.setText(getString(R.string.play_error, text));
        } else {
            errorText.setText("");
        }
    }

    public void play() {
        String server = serverInput.getText().toString();
        int port;
        try {
            port = Integer.parseInt(portInput.getText().toString());
        } catch (NumberFormatException e) {
            portInput.setError(formatMessage(e));
            return;
        }
        portInput.setError(null);

        if (boundService != null) {
            boundService.setPrefs(server, port, autoStartCheckBox.isChecked());
            boundService.play(server, port);
        }
    }

    public void stop() {
        if (boundService != null) {
            boundService.stop();
        }
    }

    @NonNull
    private String formatMessage(Throwable error) {
        String msg = error.getLocalizedMessage();
        return error.getClass().getName()
                + (msg == null ? "" : ": " + msg);
    }

}
